
# PRÁCTICA NRO10
+ **DOCENTE:** *Ing. David Mamani figueroa*
+ **AUXILIAR:** *Univ. Sergio Moises Apaza Caballero*
+ **ESTUDIANTE:** *Alex Jaime Beltran Tarqui*
+ **SIGLA:** *SIS-313*
+ **GRUPO:** *1*
***

💬¡Hola! Bienvenido 

## Templates de Figma 🎨

1. **Template de Bienes Raíces**: [Enlace](https://freebiesbug.com/figma-freebies/real-estate/) 🎨
2. **Template de Diseño de Interiores**: [Enlace](https://freebiesbug.com/figma-freebies/interior-design-template/) 🎨

## Proyecto en Figma 🚀

- [Link del Proyecto en Figma](https://www.figma.com/community/file/1201362801525377074) 🚀

## Repositorio Nuevo 📁

[React-with-AlexJaimeBeltranTarqui](https://gitlab.com/Alex_04/react-with-alexjaimebeltrantarqui) 📁

---

¡HASTA PRONTO! 😊






