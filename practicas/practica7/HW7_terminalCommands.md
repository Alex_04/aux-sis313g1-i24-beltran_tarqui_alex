
# PRÁCTICA NRO7
+ **DOCENTE:** *Ing. David Mamani figueroa*
+ **AUXILIAR:** *Univ. Sergio Moises Apaza Caballero*
+ **ESTUDIANTE:** *Alex Jaime Beltran Tarqui*
+ **SIGLA:** *SIS-313*
+ **GRUPO:** *1*
***
## Pregunta 1: Cómo crear una carpeta y cambiar de directorio en la terminal de Windows
- *link del video:* https://youtu.be/yki7MRRAWww
## Pregunta 3: Comandos Utilizados

### Creación de una carpeta:
- `cd C:\`: Cambiar al disco local C: en la terminal.
- `mkdir NombreDeLaCarpeta`: Crear una carpeta con el nombre especificado.

### Cambio de directorios:
- `cd NombreDeLaCarpeta`: Cambiar al directorio especificado.
- `cd ..`: Retroceder un nivel en la jerarquía de directorios.
## Pregunta 4: Cinco Comandos para la Terminal

A continuación se presentan cinco comandos comunes utilizados en la terminal tanto en Windows (Terminal o Powershell) como en Linux (bash), junto con su forma de uso:

1. Listar archivos y directorios:

- **Linux:** `ls`
- **Windows:** `dir`

2. Cambiar de directorio (`cd`):

- **Ambas Plataformas:** `cd NombreDelDirectorio`

3. Crear una carpeta (`mkdir`):

- **Ambas Plataformas:** `mkdir NombreDeLaCarpeta`

4. Eliminar archivos (`rm` en Linux, `del` en Windows):

- **Linux:** `rm NombreDelArchivo`
- **Windows:** `del NombreDelArchivo`

5. Copiar archivos:

- **Linux:** `cp RutaDelArchivoOrigen RutaDelArchivoDestino`
- **Windows:** `copy RutaDelArchivoOrigen RutaDelArchivoDestino`
***

**Estos comandos son fundamentales para la administración de archivos y directorios en la terminal tanto en Windows como en Linux.**




