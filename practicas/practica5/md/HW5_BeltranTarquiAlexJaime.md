
# PRÁCTICA NRO5
+ **DOCENTE:** *Ing. David Mamani figueroa*
+ **AUXILIAR:** *Univ. Sergio Moises Apaza Caballero*
+ **Universitario:** *Alex Jaime Beltran Tarqui*
+ **SIGLA:** *SIS-313*
+ **GRUPO:** *1*
***
## NIVELES REQUERIDOS COMPLETADOS:

![Nivel 1](/practicas/practica5/imagenes/nivel1.png)

![Nivel 2](/practicas/practica5/imagenes/nivel2.png)

![Nivel 3](/practicas/practica5/imagenes/nivel3.png)

![Nivel 4](/practicas/practica5/imagenes/nivel4.png)

![Nivel 5](/practicas/practica5/imagenes/nivel5.png)

![Nivel 22](/practicas/practica5/imagenes/nivel22.png)

![Nivel 23](/practicas/practica5/imagenes/nivel23.png)

![Nivel 24](/practicas/practica5/imagenes/nivel24.png)

![Nivel 25](/practicas/practica5/imagenes/nivel25.png)

![Nivel 26](/practicas/practica5/imagenes/nivel26.png)

![Nivel 27](/practicas/practica5/imagenes/nivel27.png)

![Nivel 28](/practicas/practica5/imagenes/nivel28.png)

![Nivel 29](/practicas/practica5/imagenes/nivel29.png)

![Nivel 30](/practicas/practica5/imagenes/nivel30.png)

![Nivel 31](/practicas/practica5/imagenes/nivel31.png)

![Nivel 32](/practicas/practica5/imagenes/nivel32.png)




