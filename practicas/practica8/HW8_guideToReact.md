
# PRÁCTICA NRO8
+ **DOCENTE:** *Ing. David Mamani figueroa*
+ **AUXILIAR:** *Univ. Sergio Moises Apaza Caballero*
+ **ESTUDIANTE:** *Alex Jaime Beltran Tarqui*
+ **SIGLA:** *SIS-313*
+ **GRUPO:** *1*
***
## Pregunta 2: 

- *link del video:* https://youtu.be/yZRHgSSsfOQ

## Pregunta 3: Comandos utilizados
1. Creación de Carpeta en Disco Local C:
   - Utilización del comando `mkdir` en la terminal.
   - Verificación con `dir` o `ls`.
   
2. Creación de Proyecto React con Next.js:
    - Comandos `node -v` y `npm -v` para verificar las versiones instaladas.
   - Uso de `npx create-next-app`.
   - Ejecución y visualización del proyecto en el navegador con `npm run dev`.

## Pregunta 4: Errores Comunes al crear el proyecto
- Problemas de conexión durante la descarga de dependencias.
- Conflictos de versiones de Node.js o npm.
- Errores de permisos de escritura en la carpeta de destino.

## Pregunta 5: Carpeta naranja en GitLab
- Carpeta naranja: posible enlace simbólico o submódulo.
- Solución: verificar y corregir configuraciones de GitLab.






## Pregunta 1:
![captura](/practicas/practica8/capturas/verificacion.png)

